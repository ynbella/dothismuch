const mongoose = require('mongoose');

const exerciseSchema = new mongoose.Schema({
    area: {
        enum: ['abs', 'arms', 'back', 'chest', 'legs', 'shoulders'],
        required: [true, 'Missing exercise target area'],
        type: String,
    },
    name: {
        required: [true, 'Missing exercise name'],
        type: String
    }
})

module.exports = mongoose.model('Exercise', exerciseSchema);
