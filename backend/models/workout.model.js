const mongoose = require('mongoose');

const workoutSchema = new mongoose.Schema({
    name: {
        required: [true, 'Missing exercise name'],
        type: String
    },
    description: {
      type: String
    },
    exercises: [
        {
            type: {
                type: mongoose.Types.ObjectId,
                ref: 'Exercise'
            },
            repetitions: {
                type: Number
            },
            sets: {
                type: Number
            },
            time: {
                type: Number
            },


            count: {
                type: Number
            },

            units: {
                enum: ['count', 'time'],
                required: true,
                type: String,
            }
        }
    ],
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: [true, 'Missing user account']
    }
}, {
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('Workout', workoutSchema);
