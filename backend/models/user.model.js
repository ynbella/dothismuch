const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const userSchema = new mongoose.Schema({
        // Authentication
        username: {
                index: true,
                lowercase: true,
                match: [/^[a-zA-Z0-9]+$/, 'Invalid username'],
                required: [true, 'Missing username'],
                type: String,
                unique: true
        },
        password: {
                type: String,
                required: [true, 'Missing password'],
        },

        // Identification
        name: {
                type: String
        },

        // Contact
        email: {
                lowercase: true,
                match: [/\S+@\S+\.\S+/, 'Invalid email'],
                required: [true, 'Missing email'],
                type: String,
                unique: true
        },
}, {
        timestamps: true,
        versionKey: false
})

userSchema.set('toJSON', {
        virtuals: true,
        versionKey: false,
        transform: function (doc, ret) {
                delete ret._id;
                delete ret.password;
        }
});

userSchema.pre('save', function (next) {
        const user = this;
        if (!user.isModified('password')) {
                return next()
        }

        bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(user.password, salt, function (err, hash) {
                        user.password = hash;
                        next();
                });
        });

})

userSchema.methods.compare = function (password) {
        return bcrypt.compareSync(password, this.password)
};

userSchema.methods.sign = function () {
        const today = new Date();
        const exp = new Date(today);
        exp.setDate(today.getDate() + 60);

        return jwt.sign({
                sub: this._id,
                exp: parseInt(exp.getTime() / 1000),
        }, process.env.TOKEN_SECRET)
}
module.exports = mongoose.model('User', userSchema);
