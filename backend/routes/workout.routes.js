const WorkoutController = require("../controllers/workout.controller");
const Authenticate = require("../middleware/authenticate.middleware")
const express = require('express');
const router = express.Router();

router.post('/:workout/:exercise', WorkoutController.add)
router.delete('/:workout/:exercise', WorkoutController.remove)


router.post('/create', Authenticate, WorkoutController.create)
router.get('/list', WorkoutController.list)
router.get('/lookup/:workout', WorkoutController.lookup)
router.get('/search', WorkoutController.search)
router.post('/update/:id', WorkoutController.update)
router.delete('/:id', WorkoutController.delete)

module.exports = router;
