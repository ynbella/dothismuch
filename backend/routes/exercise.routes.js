const ExerciseController = require("../controllers/exercise.controller");
const express = require('express');
const router = express.Router();

router.post('/create', ExerciseController.create)
router.get('/list', ExerciseController.list)
router.get('/search', ExerciseController.search)
router.post('/update/:id', ExerciseController.update)
router.delete('/:id', ExerciseController.delete)

module.exports = router;
