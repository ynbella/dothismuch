const Workout = require('../models/workout.model')

exports.add = function (req, res) {
    Workout.findOne({_id: req.params.workout})
        .exec(function (err, workout) {
            if (err || workout === null) {
                return res.status(404).json({error: 'Workout not found'});
            }
            workout.exercises.push({
                type: req.params.exercise,
                units: req.body.units
            })
            workout.save().then(doc => {
                return res.json(doc)
            }).catch(err => {
                return res.status(400).send({
                    error: 'Unable to update workout'
                })
            })
        })
}

exports.remove = function (req, res) {

}

exports.create = function (req, res) {
    const user = req.user.sub
    const workout = new Workout({
        ...req.body,
        user
    })

    workout.save()
        .then(doc => {
            return res.json(doc)
        })
        .catch(err => {
            return res.status(400).send({
                error: 'Unable to create workout'
            })
        })
}

exports.list = function (req, res) {
    Workout.find()
        .exec(function (err, docs) {
            if (err || docs === null) {
                res.status(404).json({error: 'No workouts found'})
            } else {
                res.json(docs)
            }
        })
}

exports.lookup = function (req, res) {
    Workout.findOne({_id: req.params.workout}).populate({
        path: 'exercises.exercise',
        model: 'Exercise'

    })
        .exec(function (err, docs) {
            if (err || docs === null) {
                res.status(404).json({error: 'Workout not found'})
            } else {
                res.json(docs)
            }
        })
}

exports.search = function (req, res) {

}

exports.update = function (req, res) {
    Workout.updateOne({_id: id}, req.body)
        .exec(function (err, doc) {
            if (err || doc === null) {
                res.status(404).json({error: 'Workout not found'});
            } else {
                res.json(doc);
            }
        })
}

exports.delete = function (req, res) {
    Workout.deleteOne({_id: req.params.id})
        .exec(function (err, doc) {
            if (err || doc === null) {
                res.status(404).json({error: 'Workout not found'});
            } else {
                res.json(doc);
            }
        })
}
