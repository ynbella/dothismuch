const Exercise = require('../models/exercise.model')

exports.create = function (req, res) {
    const exercise = new Exercise(req.body)

    exercise.save()
        .then(doc => {
            return res.json(doc)
        })
        .catch(err => {
            return res.status(400).send({
                error: 'Unable to create exercise'
            })
        })
}

exports.list = function (req, res) {
    Exercise.find()
        .exec(function (err, docs) {
            if (err || docs === null) {
                res.status(404).json({error: 'No exercises found'})
            } else {
                res.json(docs)
            }
        })
}

exports.search = function (req, res) {

}

exports.update = function (req, res) {
    Exercise.updateOne({_id: id}, req.body)
        .exec(function (err, doc) {
            if (err || doc === null) {
                res.status(404).json({error: 'Exercise not found'});
            } else {
                res.json(doc);
            }
        })
}

exports.delete = function (req, res) {
    Exercise.deleteOne({_id: req.params.id})
        .exec(function (err, doc) {
            if (err || doc === null) {
                res.status(404).json({error: 'Exercise not found'});
            } else {
                res.json(doc);
            }
        })
}
