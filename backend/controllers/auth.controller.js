const User = require('../models/user.model')

exports.register = function (req, res) {
    const {username, email, password} = req.body

    if (!username || !email || !password) {
        return res.status(400).json({error: 'Missing username, email, or password'})
    }

    User.findOne({$or: [{username: username}, {email: email}]})
        .exec(function (err, doc) {
            if (err || doc) {
                return res.status(400).json({error: 'User already exists'})
            }

            const user = new User(req.body)
            user.save()
                .then(doc => {
                    return res.json(doc)
                })
                .catch(() => {
                    return res.status(400).json({
                        error: 'Invalid user'
                    })
                })
        })
}

exports.login = function (req, res) {
    const {username, password} = req.body

    if (!username || !password) {
        return res.status(400).json({error: 'Missing username, or password'})
    }

    User.findOne({username: req.body.username})
        .exec(function (err, doc) {
            if (err || doc === null) {
                return res.status(404).json({error: 'User not found'})
            }

            if (!doc.compare(req.body.password)) {
                return res.status(401).json({error: 'Password not valid'})
            }

            const token = doc.sign();

            return res.json({
                ...doc.toJSON(),
                token
            });
        })
}
