const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
    const authorization = req.headers.authorization;

    if (req.url === '/auth/login' || req.url === '/auth/register') {
        return next();
    }

    if (!authorization) {
        res.status(400).json({error: "No Authorization Header"});
        return next();
    }

    let token = null;

    try {
        token = authorization?.split("Bearer ")[1];
    } catch {
        res.status(400).json({error: "Invalid Token Format"});
        return next();
    }

    if (!token) return res.status(401).json({error: "Access denied"});
    try {
        req.user = jwt.verify(token, process.env.TOKEN_SECRET);
        next();
    } catch (err) {
        res.status(400).json({error: "Invalid token"});
        next();
    }
};
