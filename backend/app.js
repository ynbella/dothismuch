const express = require('express');
const logger = require('morgan');
const app = express();
const cors = require('cors');

require('dotenv').config()

// Application-Level Middleware

// Third-Party Middleware
app.use(cors());
app.use(logger('dev'));

// Built-In Middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/auth', require('./routes/auth.routes'));
app.use('/exercise', require('./routes/exercise.routes'));
app.use('/workout', require('./routes/workout.routes'));

app.use('/user', require('./routes/user.router'));


const jwt = require('./_helpers/jwt');
app.use(jwt());


// Our error handler
const errorHandler = require('./_helpers/error-handler');
app.use(errorHandler);




// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 3030;
app.listen(port, function () {
  console.log('Server listening on port ' + port);
});
