const config = require('../config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../_helpers/database');
const User = db.User;
const PARecord = db.PArecord;


module.exports = {
    authenticate,
    getAllUsers,
    getByUsername,
    addUser,
    setGoals,
    getGoals,
    getAverages
}

async function authenticate({username, password}) {
    const user = await User.findOne({username});

    if (user.compare(password)) {
        const token = user.sign();

        return {
            ...user,
            token
        }
    }

    if (user && bcrypt.compareSync(password, user.hash)) {
        const token = jwt.sign({sub: user.id, role: user.role}, config.secret);
        return {
            ...user,
            token
        };
    }
}

async function getAllUsers() {
    //Returning the result of the promise.
    return await User.find().select('-hash');
}


async function getByUsername(username) {
    return await User.find({username: username});
}

async function addUser(userParam) {
    // validate
    if (await User.findOne({username: userParam.username})) {
        throw 'Username "' + userParam.username + '" is already taken';
    } else if (await User.findOne({email: userParam.email})) {
        throw 'Email "' + userParam.email + '" is already taken';
    }

    const user = new User(userParam);

    // save user
    await user.save();
}


// complete this function. It takes in calories and minute goal values in 'values' and saves it for a given userid (_id). Hint: use 'updateOne' from mongoose.
async function setGoals(values, userid) {
    const {caloriegoal, minutegoal} = values

    if (!caloriegoal || !minutegoal) {
        throw 'Missing calory or minute goal information'
    }

    if (!await User.updateOne({_id: userid}, {caloriegoal: caloriegoal, minutegoal: minutegoal})) {
        throw 'UserID "' + userid + '" not updated with values "' + values + ':';
    }
}


// complete this function. It should return calorie and minute goals for a given user.
async function getGoals(username) {
    let goals = await User.findOne({username: username}, {caloriegoal: 1, minutegoal: 1});

    if (goals) {
        console.log("Username: " + username + " Goals: " + goals);
        return goals;
    } else {
        throw 'Username "' + username + '" not in database';
    }
}

async function getAverages() {
    return await User.aggregate([{
        '$lookup': {
            'from': 'parecords',
                'localField': '_id',
                'foreignField': 'createdBy',
                'as': 'records'
        }
    }, {
        '$unwind': {
            'path': '$records',
                'preserveNullAndEmptyArrays': true
        }
    }, {
        '$group': {
            '_id': '$_id',
                'username': {
                '$first': '$username'
            },
            'first': {
                '$first': '$firstName'
            },
            'last': {
                '$first': '$lastName'
            },
            'avgcalories': {
                '$avg': '$records.calories'
            },
            'avgminutes': {
                '$avg': '$records.minutes'
            },
            'calgoal': {
                '$first': '$caloriegoal'
            },
            'minutegoal': {
                '$first': '$minutegoal'
            }
        }
    }])
}
