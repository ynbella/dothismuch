import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {CreateComponent} from './create/create.component';
import {BrowseComponent} from './browse/browse.component';
import {SettingsComponent} from './settings/settings.component';

import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';

import {AuthGuard} from './_services/auth-guard.service';


// add the route to the 'settings' component.

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},

  {path: 'create', component: CreateComponent},
  {path: 'browse', component: BrowseComponent},
  {path: 'settings', component: SettingsComponent},


  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},

  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
