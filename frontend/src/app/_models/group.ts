import {Area} from "./area";


export class Group {
  name: string;
  description: string;
  image: string;
  area: Area;

  constructor(name: string, description: string, image: string, area: Area) {

    this.name = name;
    this.description = description;
    this.image = image;
    this.area = area;
  }
}
