import { Role } from './role';

export class User {
  username: string;
  firstName: string;
  lastName: string;
  role: Role;
  token?: string;
  caloriegoal: number;
  minutegoal: number;
}
