import {Exercise} from './exercise';

export class Workout {
  author: string;
  name: string;
  exercises: Exercise[];

  constructor(author: string, name: string, exercises: Exercise[]) {
    this.author = author;
    this.name = name;
    this.exercises = exercises;
  }
}
