
export enum Area {
  Abdominals,
  Arms,
  Back,
  Chest,
  Legs,
  Shoulders
}
