import {Unit} from "./unit";
import {Area} from "./area";

export class Exercise {
  name: string;
  sets: number;
  count: number;
  unit: Unit;
  area: Area;

  constructor(name: string, sets: number, count: number, unit: Unit, area: Area) {
    this.name = name;
    this.sets = sets;
    this.count = count;
    this.unit = unit;
    this.area = area;
  }
}
