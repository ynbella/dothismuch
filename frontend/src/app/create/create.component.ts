import {Component, OnInit} from '@angular/core';
import {Group} from '../_models/group';
import {Area} from '../_models/area';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  groups: Group[];

  constructor() {

  }

  ngOnInit(): void {
    this.groups = [];
    this.groups.push(new Group('Abdominals', 'To be determined', 'assets/images/abs.png', Area.Abdominals));
    this.groups.push(new Group('Arms', 'To be determined', 'assets/images/arms.png', Area.Arms));
    this.groups.push(new Group('Back', 'To be determined', 'assets/images/back.png', Area.Back));
    this.groups.push(new Group('Chest', 'To be determined', 'assets/images/chest.png', Area.Chest));
    this.groups.push(new Group('Legs', 'To be determined', 'assets/images/legs.png', Area.Legs));
    this.groups.push(new Group('Shoulders', 'To be determined', 'assets/images/shoulders.png', Area.Shoulders));
  }
}
