import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

import { MaterialModule } from './material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { SettingsComponent } from './settings/settings.component';
import {IgxAvatarModule} from 'igniteui-angular';
import {OrderByPipe} from './_pipes/OrderBy';
import {FilterPipe} from './_pipes/Filter';
import {WorkoutComponent} from './workout/workout.component';
import {CreateComponent} from './create/create.component';
import {BrowseComponent} from './browse/browse.component';
import {GroupComponent} from './group/group.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { MyworkoutsComponent } from './myworkouts/myworkouts.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GroupComponent,
    CreateComponent,
    BrowseComponent,
    SettingsComponent,
    WorkoutComponent,
    LoginComponent,
    RegisterComponent,

    OrderByPipe,
    FilterPipe,
    ExerciseComponent,
    MyworkoutsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IgxAvatarModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
