import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform {
  transform(items, column, searchTerm) {
    const filteredList = [];
    if (column && searchTerm) {
      const newSearchTerm = !isNaN(searchTerm) ? searchTerm.toString() : searchTerm.toString().toUpperCase();
      let prop;
      return items.filter(item => {
        prop = isNaN(item[column]) ? item[column].toString().toUpperCase() : item[column].toString();
        if (prop.indexOf(newSearchTerm) < 0) {
          filteredList.push(item);
          return filteredList;
        }
      });
    } else {
      return items;
    }
  }
}

export let FILTER_PROVIDERS = [
  FilterPipe
];
