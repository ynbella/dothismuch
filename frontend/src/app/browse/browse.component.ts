import { Component, OnInit } from '@angular/core';
import {Workout} from '../_models/workout';
import {Exercise} from '../_models/exercise';
import {Unit} from "../_models/unit";

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {
  workouts: Workout[] = [];


  constructor() { }

  getRandomInt(max: number):number {
    return Math.floor(Math.random() * max);
  }

  randomExercise(): Exercise {

    let timed = this.getRandomInt(1);
    let sets = this.getRandomInt(4);
    let count = timed == 1 ? this.getRandomInt(60) : this.getRandomInt(12);
    let unit = timed == 1 ? Unit.Seconds : Unit.Reps;
    let area = this.getRandomInt(5);
    return new Exercise('Example exercise', sets, count, unit, area);
  }

  randomWorkout(): Workout {
    const exercises: Exercise[] = [];
    for (let i = 0; i < 3; i++)
    {
      exercises.push(this.randomExercise());
    }
    return new Workout('Example Author', 'Example Name', exercises);
  }

  ngOnInit(): void {
    for (let i = 0; i < 10; i++)
    {
      this.workouts.push(this.randomWorkout());
    }
    console.log(this.workouts);
  }

}
