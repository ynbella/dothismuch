import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {User} from '../_models/user';
import {first} from 'rxjs/operators';
import {Average} from '../_models/average';


@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<User[]>(`http://localhost:3030/user/allusers`);
  }


  register(user: User) {
    return this.http.post(`http://172.16.24.199:3030/auth/register`, user);
  }

  // add a function that will allow users to set their calorie and minute goals. The function will comuunicate with the back-end.
  setGoals(caloriegoal, minutegoal) {
    return this.http.post('http://localhost:3030/user/setgoals', {caloriegoal, minutegoal});
  }


  // add a function that will allow users to get calorie and minute goals for a specific user (this means, given a username,
  //  this function should fetch calories and minute goals for that user). The function will comuunicate with the back-end.
  getGoals(username) {
    return this.http.get<any>('http://localhost:3030/user/getgoals/' + username);
  }

  getAverages() {
    return this.http.get<Average[]>(`http://localhost:3030/user/getaverages`);
  }
}
