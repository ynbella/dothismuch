import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {AuthService} from './auth.service';


@Injectable({providedIn: 'root'})
export class PArecordService {
  constructor(private http: HttpClient, private auth: AuthService) {
  }


  delete(date: string) {
    return this.http.delete(`http://localhost:3030/parecord/${date}`);
  }
}
